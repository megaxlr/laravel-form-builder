# Laravel View Form Builder
I am sick and tired of writing my own forms and decided today was the day for a better solution, so here is what I came up with. 
One blade view, include and done.

## Dependencies
Because it's a great package in general, I am still depending on laravelcollective/html. 

## Installation
It's one blade view, so it shouldnt be much of an issue to install.
```bash
composer require laravelcollective/html 
cd LARAVEL_PROJECT/resources/views
wget https://raw.githubusercontent.com/megaxlr/laravel-form-builder/master/form.blade.php
cd -
```

## Validation
If you are using custom Requests, The inputs that generated an error will be highlighted with a help message telling the user what to do. 

## Usage
I'll use the User Model with `make:auth` as an example
```blade
@include('form', [
	'model' => $user,
	'route' => ['user.update', $user->id],
	'fields' => [
		'name' => [
			'type' => 'text',
			'placeholder' => 'Pick a unique name', // Supports translation strings
		],
		'email' => [
			'type' => 'email',
			'class' => 'custom-input-field', // Support for HTML classes
			'required' => true, // Put an asterisk after the label
		],
		'password' => [
			'label' => 'Enter a new password', // This supports translations using __() 
			'type' => 'password',
			'help' => 'Passwords keep your account secure, pick one thats at least 6 chars' // Alt text on the label, also supports translations
			'if' => auth()->user()->isAdmin(), // Conditional fields
		],
	],
])
```

## Contributing
Pull-requests on GitHub are ignored, send a merge request on the GitLab repo.

### Future plans
 - Automatically getting the attributes from a model to generate the fields list with.

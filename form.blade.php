
{{Form::model($model, ['route' => $route, 'method' => isset($method) ? $method : 'post', 'files' => isset($files)])}}
@foreach($fields as $name => $field)
    @if(isset($field['if']) && !$field['if']) {{-- Skip field if condition is not met. --}}
    @continue
    @endif
    @php
        $labelOptions = [];
        if(isset($field['help']))       $labelOptions = array_merge($labelOptions, ['alt'   => $field['help']]);
        if(isset($field['labelclass'])) $labelOptions = array_merge($labelOptions, ['class' => $field['labelclass']]);

        $type = $field['type'];
        // Add options
        $options = [];
        if(!in_array($field['type'], ['checkbox', 'radio'])) $options['class'] = isset($field['class']) ? $field['class'] : "form-control";
        if($errors->has($name)) $options['class'] .= ' is-invalid';
        if(isset($field['placeholder'])) $options['placeholder'] = __($field['placeholder']);
        $field['label'] = isset($field['label']) ? $field['label'] : $name;
    @endphp
    <div class="{{"form-group"}}">
        @if(isset($field['parentclass']))
            <div class="{{$field['parentclass']}}">
        @endif

        @switch($field['type'])
            @case('checkbox')
            @case('radio')
                {{Form::$type($name, isset($field['value']) ? $field['value'] : null, null, $options)}}
                @break
            @case('file')
                {{Form::file($name, $options)}}
        @endswitch
        {{Form::label($name, __(ucfirst($field['label'])), $labelOptions)}}
        @if(isset($field['required']) && $field['required'])
            <span class="text-danger" title="@lang('Required')">
                *
            </span>
        @endif
        @switch($type)
            @case('checkbox')
            @case('radio')
            @case('file')
                @break
            @case('select')
            {{Form::select($name, isset($field['value']) ? $field['value'] : null, null, $options)}}
            @break
            @case('password')
            {{Form::password($name, $options)}}
            @break
            @default
                {{Form::$type($name, isset($field['value']) ? $field['value'] : null, $options)}}
        @endswitch
        @if ($errors->has($name))
            <span class="invalid-feedback" role="alert">
                <strong>{{ ucfirst($errors->first($name)) }}</strong>
            </span>
        @endif
        @if(isset($field['parentclass']))
            </div>
        @endif
    </div>
@endforeach

{{Form::submit(__(isset($submit) ? $submit : 'Save'), ['class' => 'btn btn-success'])}}
{{Form::close()}}

